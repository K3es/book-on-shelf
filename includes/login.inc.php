<div class="d-flex justify-content-center container-fluid">
    <div class="card form-card bg-dark text-light margin-top-100">
        <div class="card-body">
            <form action="<?= "php/login.php"; ?>" method="POST" role="form">
                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" class="form-control" name="email" placeholder="Email address" required>
                </div>

                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" name="password" placeholder="Password" required>
                </div>

                <div class="d-flex justify-content-center container">
                    <button type="submit" class="btn btn-primary btn-md">Login</button>
                </div>

                <div class="text-center invalid"><?php if (isset($_SESSION["loginError"])) {
                        echo $_SESSION["loginError"];
                    }
                    unset($_SESSION["loginError"]) ?></div>
            </form>
        </div>
    </div>
</div>

<div class="d-flex justify-content-center container-fluid">
    <div class="card form-card bg-dark text-light margin-top-20">
        <div class="card-body">
            <h6 class="card-title text-center">Don't have an account?</h6>
            <div class="d-flex justify-content-center container">
                <a href="/bookonshelf/index.php?page=register" class="btn btn-primary">Create one now!</a>
            </div>
        </div>
    </div>
</div>