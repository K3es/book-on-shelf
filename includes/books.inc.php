<?php

require "classes/Book.php";

$bookClass = new Book();
$bookObject = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $event = $_POST["event"];
    $bookId = $_POST["bookId"] ?? 1;
    $bookObject = $bookClass->readBookById($bookId);
}

?>

    <div class="d-flex justify-content-center container-fluid">
        <div class="card bg-dark text-light margin-top-100 table-responsive-sm">
            <div class="card-body text-nowrap">
                <?php if (isset($_SESSION["isAdmin"]) && $_SESSION["isAdmin"]) { ?>
                    <div class="text-right">
                        <form class="btn-group-sm" action="<?= $_SERVER['PHP_SELF'] . "?page=books" ?>"
                              method="POST">
                            <button type="submit" name="event" value="create" class="btn btn-success"> Create new
                            </button>
                        </form>
                    </div>
                <?php } ?>
                <table class="table table-hover table-dark">
                    <thead>
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Book Name</th>
                        <th scope="col">Author</th>
                        <th scope="col">Genre</th>
                        <th scope="col">Language</th>
                        <th scope="col">Stock</th>
                        <th scope="col">Reserved</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    foreach ($bookClass->readBooks() as $book) {
                        ?>
                        <tr>
                            <th scope='row'><?= $book->bookId ?></th>
                            <td scope='row'><?= $book->bookName ?></td>
                            <td scope='row'><?= "$book->firstName $book->middleName $book->lastName" ?></td>
                            <td scope='row'><?= $book->genreName ?></td>
                            <td scope='row'><?= $book->languageName ?></td>
                            <td scope='row'><?= $book->stock ?></td>
                            <td scope='row'><?= $book->reservedStock ?></td>
                            <td scope='row'>
                                <?php if (isset($_SESSION["isAdmin"]) && $_SESSION["isAdmin"]) { ?>
                                    <form class="btn-group-sm" action="<?= $_SERVER['PHP_SELF'] . "?page=books" ?>"
                                          method="POST">
                                        <input type="hidden" name="bookId" value="<?= $book->bookId ?>"/>
                                        <button type="submit" name="event" value="update" class="btn btn-primary">Edit
                                        </button>
                                        <button type="submit" name="event" value="delete" class="btn btn-danger">Delete
                                        </button>

                                    </form>
                                <?php } else { ?>
                                <form class="btn-group-sm" action="<?= "php/book/updateBook.php" ?>" method="POST">
                                    <input type="hidden" name="bookId" value="<?= $book->bookId ?>"/>
                                    <button type="submit" name="event" value="rent"
                                            class="btn btn-primary" <?= $book->stock <= 0 ? "disabled" : "" ?>>Rent
                                    </button>
                                    <button type="submit" name="event" value="reserve"
                                            class="btn btn-primary" <?= $book->reservedStock < $book->stock ? "disabled" : "" ?>>
                                        Reserve
                                    </button>
                                    <?php } ?>
                                </form>
                            </td>
                        </tr>
                        <?php
                    }

                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>


<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if ($event == "delete") {
        ?>
        <div class="modal fade" id="userActionModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content card bg-dark text-light margin-top-100">
                    <h5 class="modal-title text-center">Delete book</h5>
                    <div class="card-body">
                        <form action="<?= "php/book/deleteBook.php" ?>" method="POST">
                            <div class="text-center container">
                                <div class="form-group">
                                    <label type="text" class="form-control"><?= $bookObject->bookName ?></label>
                                </div>

                                <input type="hidden" name="bookId" value="<?= $bookObject->bookId ?>"/>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-danger ml-2">Confirm</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $("#userActionModal").modal({backdrop: "static", keyboard: false});
            $("#userActionModal").modal("show");
        </script>
        <?php
    } else if ($event == "update") {
        ?>
        <div class="modal fade" id="userActionModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content card register-card bg-dark text-light margin-top-100">
                    <h5 class="modal-title text-center">Update book</h5>
                    <div class="card-body">
                        <form action="<?= "php/book/updateBook.php"; ?>" method="POST" role="form">
                            <div class="text-center container">
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="col">
                                            <input type="text" name="bookName" class="form-control"
                                                   value="<?= $bookObject->bookName ?>" placeholder="Name">
                                        </div>
                                    </div>
                                    <div class="form-row margin-top-10">
                                        <div class="col">
                                            <select class="form-select form-control" name="authorId">
                                                <?php

                                                foreach ($bookClass->readAuthors() as $author) {
                                                    ?>
                                                    <option value="<?= "$author->authorId" ?>" <?= ($author->authorId == $bookObject->fkAuthorId) ? "selected" : "" ?>><?= "$author->firstName $author->middleName $author->lastName" ?>
                                                    </option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-row margin-top-10">
                                        <div class="col">
                                            <select class="form-select form-control" name="genreId">
                                                <?php

                                                foreach ($bookClass->readGenres() as $genre) {
                                                    ?>
                                                    <option value="<?= $genre->genreId ?>" <?= ($genre->genreId == $bookObject->fkGenreId) ? "selected" : "" ?>><?= $genre->genreName ?>
                                                    </option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-row margin-top-10">
                                        <div class="col">
                                            <input type="text" name="isbn" class="form-control"
                                                   value="<?= $bookObject->isbn ?>" placeholder="ISBN">
                                        </div>
                                    </div>
                                    <div class="form-row margin-top-10">
                                        <div class="col">
                                            <select class="form-select form-control" name="languageId">
                                                <?php

                                                foreach ($bookClass->readLanguages() as $language) {
                                                    ?>
                                                    <option value="<?= $language->languageId ?>" <?= ($language->languageId == $bookObject->fkLanguageId) ? "selected" : "" ?>><?= $language->languageName ?>
                                                    </option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-row margin-top-10">
                                        <div class="col">
                                            <input type="text" name="pages" class="form-control"
                                                   value="<?= $bookObject->pages ?>" placeholder="Pages">

                                        </div>
                                    </div>
                                    <div class="form-row margin-top-10">
                                        <div class="col">
                                            <input type="text" name="stock" class="form-control"
                                                   value="<?= $bookObject->stock ?>" placeholder="Stock">
                                        </div>
                                    </div>
                                </div>

                                <input type="hidden" name="bookId" value="<?= $bookObject->bookId ?>"/>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-danger ml-2">Confirm</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $("#userActionModal").modal({backdrop: "static", keyboard: false});
            $("#userActionModal").modal("show");
        </script>
        <?php
    } else
        if ($event == "create") {
            ?>
            <div class="modal fade" id="userActionModal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content card register-card bg-dark text-light margin-top-100">
                        <h5 class="modal-title text-center">Create book</h5>
                        <div class="card-body">
                            <form action="<?= "php/book/createBook.php"; ?>" method="POST" role="form">
                                <div class="text-center container">
                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col">
                                                <input type="text" name="bookName" class="form-control"
                                                       placeholder="Name">
                                            </div>
                                        </div>
                                        <div class="form-row margin-top-10">
                                            <div class="col">
                                                <select class="form-select form-control" name="authorId">
                                                    <?php

                                                    foreach ($bookClass->readAuthors() as $author) {
                                                        ?>
                                                        <option value="<?= "$author->authorId" ?>"><?= "$author->firstName $author->middleName $author->lastName" ?>
                                                        </option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-row margin-top-10">
                                            <div class="col">
                                                <select class="form-select form-control" name="genreId">
                                                    <?php

                                                    foreach ($bookClass->readGenres() as $genre) {
                                                        ?>
                                                        <option value="<?= $genre->genreId ?>"><?= $genre->genreName ?>
                                                        </option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-row margin-top-10">
                                            <div class="col">
                                                <input type="text" name="isbn" class="form-control" placeholder="ISBN">
                                            </div>
                                        </div>
                                        <div class="form-row margin-top-10">
                                            <div class="col">
                                                <select class="form-select form-control" name="languageId">
                                                    <?php

                                                    foreach ($bookClass->readLanguages() as $language) {
                                                        ?>
                                                        <option value="<?= $language->languageId ?>"><?= $language->languageName ?>
                                                        </option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-row margin-top-10">
                                            <div class="col">
                                                <input type="text" name="pages" class="form-control"
                                                       placeholder="Pages">

                                            </div>
                                        </div>
                                        <div class="form-row margin-top-10">
                                            <div class="col">
                                                <input type="text" name="stock" class="form-control"
                                                       placeholder="Stock">
                                            </div>
                                        </div>
                                    </div>

                                    <input type="hidden" name="bookId" value="<?= $bookObject->bookId ?>"/>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                    <button type="submit" class="btn btn-danger ml-2">Confirm</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <script>
                $("#userActionModal").modal({backdrop: "static", keyboard: false});
                $("#userActionModal").modal("show");
            </script>
            <?php
        }
}
?>