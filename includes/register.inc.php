<div class="d-flex justify-content-center container-fluid">
    <div class="card register-card bg-dark text-light margin-top-100">
        <div class="card-body">
            <form action="<?= "php/user/createUser.php"; ?>" method="POST" role="form">
                <div id="carouselIndicators" class="carousel carousel-fade">
                    <div class="carousel-inner col">
                        <div class="carousel-item active">
                            <div class="form-group">
                                <label for="fullName">Full name</label>
                                <div class="form-row">
                                    <div class="col-6">
                                        <input type="text" class="form-control" name="firstName"
                                               placeholder="First name" required>
                                    </div>
                                    <div class="col">
                                        <input type="text" class="form-control" name="middleName"
                                               placeholder="Middle name">
                                    </div>
                                </div>
                                <div class="form-row margin-top-10">
                                    <div class="col">
                                        <input type="text" class="form-control" name="lastName"
                                               placeholder="Last name" required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="gender">Gender</label>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="gender" value="1" required>
                                    <label class="form-check-label" for="maleGender">
                                        Male
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="gender" value="0" required>
                                    <label class="form-check-label" for="femaleGender">
                                        Female
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="birthday">Birthday</label>
                                <input type="date" class="form-control" name="birthday" required>
                            </div>

                            <div class="margin-top-92"></div>
                        </div>
                        <div class="carousel-item">
                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input type="email" class="form-control" name="email" placeholder="Email address"
                                       required>
                            </div>

                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" name="password" placeholder="Password"
                                       required>
                            </div>

                            <div class="form-group">
                                <label for="city">Home address</label>
                                <input type="text" class="form-control" name="city" placeholder="City" required>

                                <div class="form-row margin-top-10">
                                    <div class="col-7">
                                        <input type="text" class="form-control" name="streetName"
                                               placeholder="Street name" required>
                                    </div>
                                    <div class="col-5">
                                        <input type="text" class="form-control" name="homeAddress"
                                               placeholder="Number" required>
                                    </div>
                                </div>
                                <input type="text" class="form-control margin-top-10" name="zipCode"
                                       placeholder="Zip code"
                                       required>
                            </div>

                            <div class="text-center container">
                                <button type="submit" class="btn btn-primary btn-md">Register</button>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-indicators pagination">
                        <li data-target="#carouselIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselIndicators" data-slide-to="1"></li>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="d-flex justify-content-center container-fluid">
    <div class="card form-card bg-dark text-light margin-top-20">
        <div class="card-body">
            <h6 class="card-title text-center">Already have an account?</h6>
            <div class="d-flex justify-content-center container">
                <a href="/bookonshelf/index.php?page=login" class="btn btn-primary">Login</a>
            </div>
        </div>
    </div>
</div>