<nav class="navbar navbar-expand-md fixed-top navbar-dark bg-dark">
    <div>
        <a class="navbar-brand ml-auto" href="#">BookOnShelf</a>
    </div>
    <div class="navbar-collapse collapse w-100 order-1 order-md-0 nav-collapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="/bookonshelf/index.php?page=home">Home</a>
            </li>
            <?php
            if ($_SESSION["loggedIn"] ?? false) { ?>
                <li class="nav-item">
                    <a class="nav-link" href="/bookonshelf/index.php?page=books">Books</a>
                </li>

                <?php

                if ($_SESSION["isAdmin"] ?? false) {
                    ?>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                           data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">Admin</a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="/bookonshelf/index.php?page=users">Users</a>
                            <a class="dropdown-item" href="/bookonshelf/index.php?page=genres">Genres</a>
                            <a class="dropdown-item" href="/bookonshelf/index.php?page=authors">Authors</a>
                            <a class="dropdown-item" href="/bookonshelf/index.php?page=languages">Languages</a>
                        </div>
                    </li>
                <?php } else { ?>
                    <li class="nav-item">
                        <a class="nav-link" href="/bookonshelf/index.php?page=rentedbooks">Rented Books</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/bookonshelf/index.php?page=reservedbooks">Reserved Books</a>
                    </li>
                <?php }
            }
            ?>
        </ul>
    </div>
    <div class="navbar-collapse collapse w-100 order-3 nav-collapse">
        <ul class="navbar-nav ml-auto">
            <?php
            if ($_SESSION["loggedIn"] ?? false) { ?>
                <li class="nav-item">
                    <a class="nav-link" href="/bookonshelf/php/logout.php">Logout</a>
                </li>
                <?php
            } else { ?>
                <li class="nav-item">
                    <a class="nav-link" href="/bookonshelf/index.php?page=login">Login</a>
                </li>
                <?php
            } ?>
        </ul>
    </div>
    <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target=".nav-collapse">
        <span class="navbar-toggler-icon"></span>
    </button>
</nav>