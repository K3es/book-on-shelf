<?php

require "classes/Book.php";

$bookClass = new Book();
$bookObject = "";
$userId = $_SESSION["loggedUserId"];

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $event = $_POST["event"];
    $bookId = $_POST["bookId"] ?? 1;
    $bookObject = $bookClass->readBookById($bookId);
}

?>

<div class="d-flex justify-content-center container-fluid">
    <div class="card bg-dark text-light margin-top-100 table-responsive-sm">
        <div class="card-body text-nowrap">
            <table class="table table-hover table-dark">
                <thead>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Book Name</th>
                    <th scope="col">Genre</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php

                foreach ($bookClass->readReservedBooksById($userId) as $book) {
                    ?>
                    <tr>
                        <th scope='row'><?= $book->reserveId ?></th>
                        <td scope='row'><?= $book->bookName ?></td>
                        <td scope='row'><?= $book->genreName ?></td>
                        <td scope='row'>
                            <form class="btn-group-sm" action="<?= "php/book/deleteBook.php" ?>" method="POST">
                                <input type="hidden" name="reserveId" value="<?= $book->reserveId ?>"/>
                                <input type="hidden" name="bookId" value="<?= $book->bookId ?>"/>
                                <button type="submit" name="event" value="cancel" class="btn btn-danger">Cancel</button>
                            </form>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>