<?php

require "classes/User.php";

$user = new User();
$userObject = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $event = $_POST["event"];
    $userId = $_POST["userId"] ?? 1;
    $userObject = $user->readUserById($userId);

    if ($event == "role") {
        $data = array(
            "isAdmin" => $userObject->isAdmin ? 0 : 1
        );

        $user->updateUser($userId, $data);
    }
}

?>

    <div class="d-flex justify-content-center container-fluid">
        <div class="card bg-dark text-light margin-top-100 table-responsive-sm">
            <div class="card-body text-nowrap">
                <div class="text-right">
                    <form class="btn-group-sm" action="<?= $_SERVER['PHP_SELF'] . "?page=users" ?>" method="POST">
                        <button type="submit" name="event" value="create" class="btn btn-success">Create new</button>
                    </form>
                </div>
                <table class="table table-hover table-dark">
                    <thead>
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Full Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Role</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    foreach ($user->readUsers() as $user) {
                        ?>
                        <tr>
                            <th scope='row'><?= $user->userId ?></th>
                            <td scope='row'><?= "$user->firstName $user->middleName $user->lastName" ?></td>
                            <td scope='row'><?= $user->email ?></td>
                            <td scope='row'><?= $user->isAdmin ?></td>
                            <td scope='row'>
                                <form class="btn -group-sm" action="<?= $_SERVER['PHP_SELF'] . "?page=users" ?>" method="POST">
                                    <input type="hidden" name="userId" value="<?= $user->userId ?>"/>
                                    <button type="submit" name="event" value="update" class="btn btn-primary" <?= ($_SESSION["loggedUserId"] == $user->userId) ? "disabled" : ""; ?>>Edit</button>
                                    <button type="submit" name="event" value="delete" class="btn btn-danger" <?= ($_SESSION["loggedUserId"] == $user->userId) ? "disabled" : ""; ?>>Delete</button>
                                    <button type="submit" name="event" value="role" class="btn btn-<?= ($user->isAdmin) ? "success" : "secondary"; ?>" <?= ($_SESSION["loggedUserId"] == $user->userId) ? "disabled" : ""; ?>>Admin</button>
                                </form>
                            </td>
                        </tr>
                        <?php
                    }

                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>


<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if ($event == "delete") {
        ?>
        <div class="modal fade" id="userActionModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content card bg-dark text-light margin-top-100">
                    <h5 class="modal-title text-center">Delete user</h5>
                    <div class="card-body">
                        <form action="<?= "php/user/deleteUser.php" ?>" method="POST">
                            <div class="text-center container">
                                <div class="form-group">
                                    <label type="text" class="form-control"><?= $userObject->email ?></label>
                                </div>

                                <input type="hidden" name="userId" value="<?= $userObject->userId ?>"/>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-danger ml-2">Confirm</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $("#userActionModal").modal({backdrop: "static", keyboard: false});
            $("#userActionModal").modal("show");
        </script>
        <?php
    } else if ($event == "update") {
        ?>
        <div class="modal fade" id="userActionModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content card register-card bg-dark text-light margin-top-100">
                    <div class="card-body">
                        <form action="<?= "php/user/updateUser.php"; ?>" method="POST" role="form">
                            <input type="hidden" name="userId" value="<?= $userObject->userId ?>"/>
                            <div id="carouselIndicators" class="carousel carousel-fade">
                                <div class="carousel-inner col">
                                    <div class="carousel-item active">
                                        <div class="form-group">
                                            <label for="fullName">Full name</label>
                                            <div class="form-row">
                                                <div class="col-6">
                                                    <input type="text" class="form-control" name="firstName"
                                                           value="<?= $userObject->firstName ?>"
                                                           placeholder="First name" required>
                                                </div>
                                                <div class="col">
                                                    <input type="text" class="form-control" name="middleName"
                                                           value="<?= $userObject->middleName ?>"
                                                           placeholder="Middle name">
                                                </div>
                                            </div>
                                            <div class="form-row margin-top-10">
                                                <div class="col">
                                                    <input type="text" class="form-control" name="lastName"
                                                           value="<?= $userObject->lastName ?>"
                                                           placeholder="Last name" required>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="gender">Gender</label>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="gender"
                                                       value="1" required <?php if ($userObject->gender) {
                                                    echo "checked";
                                                } ?>>
                                                <label class="form-check-label" for="maleGender">Male</label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="gender"
                                                       value="0" required <?php if (!$userObject->gender) {
                                                    echo "checked";
                                                } ?>>
                                                <label class="form-check-label" for="femaleGender">Female</label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="birthday">Birthday</label>
                                            <input type="date" class="form-control" name="birthday"
                                                   value="<?= $userObject->birthday ?>" required>
                                        </div>


                                        <div class="margin-top-92"></div>
                                    </div>
                                    <div class="carousel-item">
                                        <div class="form-group">
                                            <label for="email">Email address</label>
                                            <input type="email" class="form-control" name="email"
                                                   value="<?= $userObject->email ?>"
                                                   placeholder="Email address" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="password">Password</label>
                                            <input type="password" class="form-control" name="password"
                                                   value="<?= $userObject->password ?>"
                                                   placeholder="Password" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="city">Home address</label>
                                            <input type="text" class="form-control" name="city"
                                                   value="<?= $userObject->cityName ?>"
                                                   placeholder="City" required>

                                            <div class="form-row margin-top-10">
                                                <div class="col-7">
                                                    <input type="text" class="form-control" name="streetName"
                                                           value="<?= $userObject->streetName ?>"
                                                           placeholder="Street name" required>
                                                </div>
                                                <div class="col-5">
                                                    <input type="text" class="form-control" name="homeAddress"
                                                           value="<?= $userObject->homeAddress ?>"
                                                           placeholder="Number" required>
                                                </div>
                                            </div>
                                            <input type="text" class="form-control margin-top-10" name="zipCode"
                                                   value="<?= $userObject->zipCode ?>"
                                                   placeholder="Zip code" required>
                                        </div>

                                        <div class="d-flex justify-content-center container">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                Cancel
                                            </button>
                                            <button type="submit" class="btn btn-danger ml-2">Confirm</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="carousel-indicators pagination">
                                    <li data-target="#carouselIndicators" data-slide-to="0" class="active"></li>
                                    <li data-target="#carouselIndicators" data-slide-to="1"></li>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $("#userActionModal").modal({backdrop: "static", keyboard: false});
            $("#userActionModal").modal("show");
        </script>
        <?php
    } else if ($event == "create") {
        ?>
        <div class="modal fade" id="userActionModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content card register-card bg-dark text-light margin-top-100">
                    <div class="card-body">
                        <form action="<?= "php/user/createUser.php"; ?>" method="POST" role="form">
                            <input type="hidden" name="isAdmin" value="true"/>
                            <div id="carouselIndicators" class="carousel carousel-fade">
                                <div class="carousel-inner col">
                                    <div class="carousel-item active">
                                        <div class="form-group">
                                            <label for="fullName">Full name</label>
                                            <div class="form-row">
                                                <div class="col-6">
                                                    <input type="text" class="form-control" name="firstName"
                                                           placeholder="First name" required>
                                                </div>
                                                <div class="col">
                                                    <input type="text" class="form-control" name="middleName"
                                                           placeholder="Middle name">
                                                </div>
                                            </div>
                                            <div class="form-row margin-top-10">
                                                <div class="col">
                                                    <input type="text" class="form-control" name="lastName"
                                                           placeholder="Last name" required>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="gender">Gender</label>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="gender"
                                                       value="1" required>
                                                <label class="form-check-label" for="maleGender">Male</label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="gender"
                                                       value="0" required>
                                                <label class="form-check-label" for="femaleGender">Female</label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="birthday">Birthday</label>
                                            <input type="date" class="form-control" name="birthday" required>
                                        </div>


                                        <div class="margin-top-92"></div>
                                    </div>
                                    <div class="carousel-item">
                                        <div class="form-group">
                                            <label for="email">Email address</label>
                                            <input type="email" class="form-control" name="email"
                                                   placeholder="Email address" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="password">Password</label>
                                            <input type="password" class="form-control" name="password"
                                                   placeholder="Password" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="city">Home address</label>
                                            <input type="text" class="form-control" name="city"
                                                   placeholder="City" required>

                                            <div class="form-row margin-top-10">
                                                <div class="col-7">
                                                    <input type="text" class="form-control" name="streetName"
                                                           placeholder="Street name" required>
                                                </div>
                                                <div class="col-5">
                                                    <input type="text" class="form-control" name="homeAddress"
                                                           placeholder="Number" required>
                                                </div>
                                            </div>
                                            <input type="text" class="form-control margin-top-10" name="zipCode"
                                                   placeholder="Zip code" required>
                                        </div>

                                        <div class="d-flex justify-content-center container">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                Cancel
                                            </button>
                                            <button type="submit" class="btn btn-danger ml-2">Confirm</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="carousel-indicators pagination">
                                    <li data-target="#carouselIndicators" data-slide-to="0" class="active"></li>
                                    <li data-target="#carouselIndicators" data-slide-to="1"></li>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $("#userActionModal").modal({backdrop: "static", keyboard: false});
            $("#userActionModal").modal("show");
        </script>
        <?php
    }
}
?>