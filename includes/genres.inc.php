<?php

require "classes/Genre.php";

$genre = new Genre();
$genreObject = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $event = $_POST["event"];
    $genreId = $_POST["genreId"] ?? 1;
    $genreObject = $genre->readGenreById($genreId);
}

?>

    <div class="d-flex justify-content-center container-fluid">
        <div class="card bg-dark text-light margin-top-100 table-responsive-sm">
            <div class="card-body text-nowrap">
                <div class="text-right">
                    <form class="btn-group-sm" action="<?= $_SERVER['PHP_SELF'] . "?page=genres" ?>" method="POST">
                        <button type="submit" name="event" value="create" class="btn btn-success">Create new</button>
                    </form>
                </div>
                <table class="table table-hover table-dark">
                    <thead>
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Genre Name</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    foreach ($genre->readGenres() as $genre) {
                        ?>
                        <tr>
                            <th scope='row'><?= $genre->genreId ?></th>
                            <td scope='row'><?= "$genre->genreName" ?></td>
                            <td scope='row'>
                                <form class="btn-group-sm" action="<?= $_SERVER['PHP_SELF'] . "?page=genres" ?>" method="POST">
                                    <input type="hidden" name="genreId" value="<?= $genre->genreId ?>"/>
                                    <button type="submit" name="event" value="update" class="btn btn-primary">Edit</button>
                                    <button type="submit" name="event" value="delete" class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                        <?php
                    }

                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>


<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if ($event == "delete") {
        ?>
        <div class="modal fade" id="userActionModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content card bg-dark text-light margin-top-100">
                    <h5 class="modal-title text-center">Delete genre</h5>
                    <div class="card-body">
                        <form action="<?= "php/genre/deleteGenre.php" ?>" method="POST">
                            <div class="text-center container">
                                <div class="form-group">
                                    <label type="text" class="form-control"><?= $genreObject->genreName ?></label>
                                </div>

                                <input type="hidden" name="genreId" value="<?= $genreObject->genreId ?>"/>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-danger ml-2">Confirm</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $("#userActionModal").modal({backdrop: "static", keyboard: false});
            $("#userActionModal").modal("show");
        </script>
        <?php
    } else if ($event == "update") {
        ?>
        <div class="modal fade" id="userActionModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content card bg-dark text-light margin-top-100">
                    <h5 class="modal-title text-center">Update genre</h5>
                    <div class="card-body">
                        <form action="<?= "php/genre/updateGenre.php"; ?>" method="POST" role="form">
                            <div class="text-center container">
                                <div class="form-group">
                                    <input type="text" name="genreName" class="form-control" value="<?= $genreObject->genreName ?>" placeholder="Genre name">
                                </div>

                                <input type="hidden" name="genreId" value="<?= $genreObject->genreId ?>"/>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-danger ml-2">Confirm</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $("#userActionModal").modal({backdrop: "static", keyboard: false});
            $("#userActionModal").modal("show");
        </script>
        <?php
    } else if ($event == "create") {
        ?>
        <div class="modal fade" id="userActionModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content card bg-dark text-light margin-top-100">
                    <h5 class="modal-title text-center">Create genre</h5>
                    <div class="card-body">
                        <form action="<?= "php/genre/createGenre.php"; ?>" method="POST" role="form">
                            <div class="text-center container">
                                <div class="form-group">
                                    <input type="text" name="genreName" class="form-control" placeholder="Genre name">
                                </div>

                                <input type="hidden" name="genreId" value="<?= $genreObject->genreId ?>"/>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-danger ml-2">Confirm</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $("#userActionModal").modal({backdrop: "static", keyboard: false});
            $("#userActionModal").modal("show");
        </script>
        <?php
    }
}
?>