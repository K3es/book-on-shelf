<?php

require "classes/Language.php";

$language = new Language();
$languageObject = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $event = $_POST["event"];
    $languageId = $_POST["languageId"] ?? 1;
    $languageObject = $language->readLanguageById($languageId);
}

?>

    <div class="d-flex justify-content-center container-fluid">
        <div class="card bg-dark text-light margin-top-100 table-responsive-sm">
            <div class="card-body text-nowrap">
                <div class="text-right">
                    <form class="btn-group-sm" action="<?= $_SERVER['PHP_SELF'] . "?page=languages" ?>" method="POST">
                        <button type="submit" name="event" value="create" class="btn btn-success">Create new</button>
                    </form>
                </div>
                <table class="table table-hover table-dark">
                    <thead>
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Language Name</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    foreach ($language->readLanguages() as $language) {
                        ?>
                        <tr>
                            <th scope='row'><?= $language->languageId ?></th>
                            <td scope='row'><?= "$language->languageName" ?></td>
                            <td scope='row'>
                                <form class="btn-group-sm" action="<?= $_SERVER['PHP_SELF'] . "?page=languages" ?>" method="POST">
                                    <input type="hidden" name="languageId" value="<?= $language->languageId ?>"/>
                                    <button type="submit" name="event" value="update" class="btn btn-primary">Edit</button>
                                    <button type="submit" name="event" value="delete" class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                        <?php
                    }

                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>


<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if ($event == "delete") {
        ?>
        <div class="modal fade" id="userActionModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content card bg-dark text-light margin-top-100">
                    <h5 class="modal-title text-center">Delete language</h5>
                    <div class="card-body">
                        <form action="<?= "php/language/deleteLanguage.php" ?>" method="POST">
                            <div class="text-center container">
                                <div class="form-group">
                                    <label type="text" class="form-control"><?= $languageObject->languageName ?></label>
                                </div>

                                <input type="hidden" name="languageId" value="<?= $languageObject->languageId ?>"/>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-danger ml-2">Confirm</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $("#userActionModal").modal({backdrop: "static", keyboard: false});
            $("#userActionModal").modal("show");
        </script>
        <?php
    } else if ($event == "update") {
        ?>
        <div class="modal fade" id="userActionModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content card bg-dark text-light margin-top-100">
                    <h5 class="modal-title text-center">Update language</h5>
                    <div class="card-body">
                        <form action="<?= "php/language/updateLanguage.php"; ?>" method="POST" role="form">
                            <div class="text-center container">
                                <div class="form-group">
                                    <input type="text" name="languageName" class="form-control" value="<?= $languageObject->languageName ?>" placeholder="Language name">
                                </div>

                                <input type="hidden" name="languageId" value="<?= $languageObject->languageId ?>"/>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-danger ml-2">Confirm</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $("#userActionModal").modal({backdrop: "static", keyboard: false});
            $("#userActionModal").modal("show");
        </script>
        <?php
    } else if ($event == "create") {
        ?>
        <div class="modal fade" id="userActionModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content card bg-dark text-light margin-top-100">
                    <h5 class="modal-title text-center">Create language</h5>
                    <div class="card-body">
                        <form action="<?= "php/language/createLanguage.php"; ?>" method="POST" role="form">
                            <div class="text-center container">
                                <div class="form-group">
                                    <input type="text" name="languageName" class="form-control" placeholder="Language name">
                                </div>

                                <input type="hidden" name="languageId" value="<?= $languageObject->languageId ?>"/>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-danger ml-2">Confirm</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $("#userActionModal").modal({backdrop: "static", keyboard: false});
            $("#userActionModal").modal("show");
        </script>
        <?php
    }
}
?>