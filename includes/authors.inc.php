<?php

require "classes/Author.php";

$author = new Author();
$authorObject = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $event = $_POST["event"];
    $authorId = $_POST["authorId"] ?? 1;
    $authorObject = $author->readAuthorById($authorId);
}

?>

    <div class="d-flex justify-content-center container-fluid">
        <div class="card bg-dark text-light margin-top-100 table-responsive-sm">
            <div class="card-body text-nowrap">
                <div class="text-right">
                    <form class="btn-group-sm" action="<?= $_SERVER['PHP_SELF'] . "?page=authors" ?>" method="POST">
                        <button type="submit" name="event" value="create" class="btn btn-success">Create new</button>
                    </form>
                </div>
                <table class="table table-hover table-dark">
                    <thead>
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Author Name</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    foreach ($author->readAuthors() as $author) {
                        ?>
                        <tr>
                            <th scope='row'><?= $author->authorId ?></th>
                            <td scope='row'><?= "$author->firstName $author->middleName $author->lastName" ?></td>
                            <td scope='row'>
                                <form class="btn-group-sm" action="<?= $_SERVER['PHP_SELF'] . "?page=authors" ?>" method="POST">
                                    <input type="hidden" name="authorId" value="<?= $author->authorId ?>"/>
                                    <button type="submit" name="event" value="update" class="btn btn-primary">Edit</button>
                                    <button type="submit" name="event" value="delete" class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                        <?php
                    }

                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>


<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if ($event == "delete") {
        ?>
        <div class="modal fade" id="userActionModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content card bg-dark text-light margin-top-100">
                    <h5 class="modal-title text-center">Delete author</h5>
                    <div class="card-body">
                        <form action="<?= "php/author/deleteAuthor.php" ?>" method="POST">
                            <div class="text-center container">
                                <div class="form-group">
                                    <label type="text" class="form-control"><?= "$authorObject->firstName $authorObject->middleName $authorObject->lastName" ?></label>
                                </div>

                                <input type="hidden" name="authorId" value="<?= $authorObject->authorId ?>"/>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-danger ml-2">Confirm</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $("#userActionModal").modal({backdrop: "static", keyboard: false});
            $("#userActionModal").modal("show");
        </script>
        <?php
    } else if ($event == "update") {
        ?>
        <div class="modal fade" id="userActionModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content card bg-dark text-light margin-top-100">
                    <h5 class="modal-title text-center">Update author</h5>
                    <div class="card-body">
                        <form action="<?= "php/author/updateAuthor.php"; ?>" method="POST" role="form">
                            <div class="text-center container">
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="col-6">
                                            <input type="text" class="form-control" name="firstName"
                                                   value="<?= $authorObject->firstName ?>"
                                                   placeholder="First name" required>
                                        </div>
                                        <div class="col">
                                            <input type="text" class="form-control" name="middleName"
                                                   value="<?= $authorObject->middleName ?>"
                                                   placeholder="Middle name">
                                        </div>
                                    </div>
                                    <div class="form-row margin-top-10">
                                        <div class="col">
                                            <input type="text" class="form-control" name="lastName"
                                                   value="<?= $authorObject->lastName ?>"
                                                   placeholder="Last name" required>
                                        </div>
                                    </div>
                                </div>

                                <input type="hidden" name="authorId" value="<?= $authorObject->authorId ?>"/>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-danger ml-2">Confirm</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $("#userActionModal").modal({backdrop: "static", keyboard: false});
            $("#userActionModal").modal("show");
        </script>
        <?php
    } else if ($event == "create") {
        ?>
        <div class="modal fade" id="userActionModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content card bg-dark text-light margin-top-100">
                    <h5 class="modal-title text-center">Create author</h5>
                    <div class="card-body">
                        <form action="<?= "php/author/createAuthor.php"; ?>" method="POST" role="form">
                            <div class="text-center container">
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="col-6">
                                            <input type="text" class="form-control" name="firstName"
                                                   placeholder="First name" required>
                                        </div>
                                        <div class="col">
                                            <input type="text" class="form-control" name="middleName"
                                                   placeholder="Middle name">
                                        </div>
                                    </div>
                                    <div class="form-row margin-top-10">
                                        <div class="col">
                                            <input type="text" class="form-control" name="lastName"
                                                   placeholder="Last name" required>
                                        </div>
                                    </div>
                                </div>

                                <input type="hidden" name="authorId" value="<?= $authorObject->authorId ?>"/>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-danger ml-2">Confirm</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $("#userActionModal").modal({backdrop: "static", keyboard: false});
            $("#userActionModal").modal("show");
        </script>
        <?php
    }
}
?>