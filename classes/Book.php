<?php

if (file_exists("libraries/Database.php")) {
    require "libraries/Database.php";
} else if (file_exists("../libraries/Database.php")) {
    require "../libraries/Database.php";
} else if (file_exists("../../libraries/Database.php")) {
    require "../../libraries/Database.php";
}

class Book
{
    private $database;

    public function __construct()
    {
        $this->database = new Database;
    }

    public function readBooks()
    {
        $this->database->query("SELECT * FROM (((books
                                    INNER JOIN authors ON books.fkAuthorId = authors.authorId)
                                    INNER JOIN genres ON books.fkGenreId = genres.genreId)
                                    INNER JOIN languages ON books.fkLanguageId = languages.languageId)
                                    ORDER BY bookId ASC");

        $rows = $this->database->resultSet();

        if ($this->database->rowCount() > 0) {
            return $rows;
        } else {
            return array();
        }
    }

    public function readGenres()
    {
        $this->database->query("SELECT * FROM genres");

        $rows = $this->database->resultSet();

        if ($this->database->rowCount() > 0) {
            return $rows;
        } else {
            return array();
        }
    }

    public function readLanguages()
    {
        $this->database->query("SELECT * FROM languages");

        $rows = $this->database->resultSet();

        if ($this->database->rowCount() > 0) {
            return $rows;
        } else {
            return array();
        }
    }

    public function readAuthors()
    {
        $this->database->query("SELECT * FROM authors");

        $rows = $this->database->resultSet();

        if ($this->database->rowCount() > 0) {
            return $rows;
        } else {
            return array();
        }
    }

    public function readRentedBooksById($userId)
    {
        $this->database->query("SELECT * FROM (((rentedbooks
                                    INNER JOIN users ON rentedbooks.fkUserId = users.userId)
                                    INNER JOIN books ON rentedbooks.fkBookId = books.bookId)
                                    INNER JOIN genres ON books.fkGenreId = genres.genreId)
                                    WHERE fkUserId = :fkUserId
                                    ORDER BY rentalId ASC");
        $this->database->bind(":fkUserId", $userId);

        $rows = $this->database->resultSet();

        if ($this->database->rowCount() > 0) {
            return $rows;
        } else {
            return array();
        }
    }

    public function readReservedBooksById($userId)
    {
        $this->database->query("SELECT * FROM (((reservedbooks
                                    INNER JOIN users ON reservedbooks.fkUserId = users.userId)
                                    INNER JOIN books ON reservedbooks.fkBookId = books.bookId)
                                    INNER JOIN genres ON books.fkGenreId = genres.genreId)
                                    WHERE fkUserId = :fkUserId
                                    ORDER BY reserveId ASC");
        $this->database->bind(":fkUserId", $userId);

        $rows = $this->database->resultSet();

        if ($this->database->rowCount() > 0) {
            return $rows;
        } else {
            return array();
        }
    }

    public function readReservedBooks($bookId)
    {
        $this->database->query("SELECT * FROM (((reservedbooks
                                    INNER JOIN users ON reservedbooks.fkUserId = users.userId)
                                    INNER JOIN books ON reservedbooks.fkBookId = books.bookId)
                                    INNER JOIN genres ON books.fkGenreId = genres.genreId)
                                    WHERE fkBookId = :fkBookId
                                    ORDER BY reserveId ASC");
        $this->database->bind(":fkBookId", $bookId);

        $rows = $this->database->resultSet();

        if ($this->database->rowCount() > 0) {
            return $rows;
        } else {
            return array();
        }
    }

    public function readBookById($bookId)
    {
        $this->database->query("SELECT * FROM books WHERE bookId = :bookId");
        $this->database->bind(":bookId", $bookId);

        $row = $this->database->resultSingle();

        if ($this->database->rowCount() > 0) {
            return $row;
        } else {
            return false;
        }
    }

    public function deleteBook($bookId)
    {
        $this->database->query("DELETE FROM books WHERE bookId = :bookId");
        $this->database->bind(":bookId", $bookId);

        $this->database->execute();
    }

    public function deleteRental($rentalId)
    {
        $this->database->query("DELETE FROM rentedbooks WHERE rentalId = :rentalId");
        $this->database->bind(":rentalId", $rentalId);

        $this->database->execute();
    }

    public function deleteReserve($reserveId)
    {
        $this->database->query("DELETE FROM reservedbooks WHERE reserveid = :reserveId");
        $this->database->bind(":reserveId", $reserveId);

        $this->database->execute();
    }

    public function updateBook($bookId, $data)
    {
        $sqlSet = "";

        foreach ($data as $key => $value) {
            if (!is_null($value)) {
                $sqlSet = "$sqlSet$key = :$key, ";
            }
        }

        $sqlSet = rtrim($sqlSet, ", ");

        $sql = "UPDATE books SET $sqlSet WHERE bookId = :bookId";

        $this->database->query($sql);

        foreach ($data as $key => $value) {
            $this->database->bind(":$key", $value);
        }

        $this->database->bind(":bookId", $bookId);

        if ($this->database->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function createRental($bookId, $userId) {
        $sql = "INSERT INTO rentedbooks (fkBookId, fkUserId, rentDate, expirationDate) VALUES (:fkBookId, :fkUserId, :rentDate, :expirationDate)";

        $this->database->query($sql);

        $this->database->bind(":fkBookId", $bookId);
        $this->database->bind(":fkUserId", $userId);
        $this->database->bind(":rentDate", date("Y-m-d"));
        $this->database->bind(":expirationDate", date("Y-m-d", strtotime("+14 day")));

        if ($this->database->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function createReserve($bookId, $userId) {
        $sql = "INSERT INTO reservedbooks (fkBookId, fkUserId) VALUES (:fkBookId, :fkUserId)";

        $this->database->query($sql);

        $this->database->bind(":fkBookId", $bookId);
        $this->database->bind(":fkUserId", $userId);

        if ($this->database->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function createBook($data)
    {
        $sqlKeys = "";
        $sqlValues = "";
        $dataTypes = array(
            "bookName",
            "fkAuthorId",
            "fkGenreId",
            "isbn",
            "fkLanguageId",
            "pages",
            "stock",
            "usedStock",
            "reservedStock"
        );

        foreach ($dataTypes as $value) {
            $sqlKeys = "$sqlKeys$value, ";
            $sqlValues = "$sqlValues:$value, ";
        }

        $sqlKeys = rtrim($sqlKeys, ", ");
        $sqlKeys = "($sqlKeys)";

        $sqlValues = rtrim($sqlValues, ", ");
        $sqlValues = "($sqlValues)";

        $sql = "INSERT INTO books $sqlKeys VALUES $sqlValues";

        $this->database->query($sql);

        foreach ($dataTypes as $value) {
            $this->database->bind(":$value", $data["$value"]);
        }

        if ($this->database->execute()) {
            return true;
        } else {
            return false;
        }
    }
}