<?php

if (file_exists("libraries/Database.php")) {
    require "libraries/Database.php";
} else if (file_exists("../libraries/Database.php")) {
    require "../libraries/Database.php";
} else if (file_exists("../../libraries/Database.php")) {
    require "../../libraries/Database.php";
}

class Genre
{
    private $database;

    public function __construct()
    {
        $this->database = new Database;
    }

    public function readGenres()
    {
        $this->database->query("SELECT * FROM genres");

        $rows = $this->database->resultSet();

        if ($this->database->rowCount() > 0) {
            return $rows;
        } else {
            return array();
        }
    }

    public function readGenreById($genreId)
    {
        $this->database->query("SELECT * FROM genres WHERE genreId = :genreId");
        $this->database->bind(":genreId", $genreId);

        $row = $this->database->resultSingle();

        if ($this->database->rowCount() > 0) {
            return $row;
        } else {
            return false;
        }
    }

    public function deleteGenre($genreId)
    {
        $this->database->query("DELETE FROM genres WHERE genreId = :genreId");
        $this->database->bind(":genreId", $genreId);

        $this->database->execute();
    }

    public function updateGenre($genreId, $genreName)
    {
        $this->database->query("UPDATE genres SET genreName = :genreName WHERE genreId = :genreId");
        $this->database->bind(":genreId", $genreId);
        $this->database->bind(":genreName", $genreName);

        if ($this->database->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function createGenre($genreName)
    {
        $this->database->query("INSERT INTO genres (genreName) VALUES (:genreName)");
        $this->database->bind(":genreName", $genreName);

        if ($this->database->execute()) {
            return true;
        } else {
            return false;
        }
    }
}