<?php

if (file_exists("libraries/Database.php")) {
    require "libraries/Database.php";
} else if (file_exists("../libraries/Database.php")) {
    require "../libraries/Database.php";
} else if (file_exists("../../libraries/Database.php")) {
    require "../../libraries/Database.php";
}

class Author
{
    private $database;

    public function __construct()
    {
        $this->database = new Database;
    }

    public function readAuthors()
    {
        $this->database->query("SELECT * FROM authors");

        $rows = $this->database->resultSet();

        if ($this->database->rowCount() > 0) {
            return $rows;
        } else {
            return array();
        }
    }

    public function readAuthorById($authorId)
    {
        $this->database->query("SELECT * FROM authors WHERE authorId = :authorId");
        $this->database->bind(":authorId", $authorId);

        $row = $this->database->resultSingle();

        if ($this->database->rowCount() > 0) {
            return $row;
        } else {
            return false;
        }
    }

    public function deleteAuthor($authorId) {
        $this->database->query("DELETE FROM authors WHERE authorId = :authorId");
        $this->database->bind(":authorId", $authorId);

        $this->database->execute();
    }


    public function updateAuthor($authorId, $data) {
        $sqlSet = "";

        foreach ($data as $key => $value) {
            if (!is_null($value)) {
                $sqlSet = "$sqlSet$key = :$key, ";
            }
        }

        $sqlSet = rtrim($sqlSet, ", ");

        $sql = "UPDATE authors SET $sqlSet WHERE authorId = :authorId";

        $this->database->query($sql);

        foreach ($data as $key => $value) {
            $this->database->bind(":$key", $value);
        }

        $this->database->bind(":authorId", $authorId);

        if ($this->database->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function createAuthor($data)
    {
        $sqlKeys = "";
        $sqlValues = "";
        $dataTypes = array(
            "firstName",
            "middleName",
            "lastName"
        );

        foreach ($dataTypes as $value) {
            $sqlKeys = "$sqlKeys$value, ";
            $sqlValues = "$sqlValues:$value, ";
        }

        $sqlKeys = rtrim($sqlKeys, ", ");
        $sqlKeys = "($sqlKeys)";

        $sqlValues = rtrim($sqlValues, ", ");
        $sqlValues = "($sqlValues)";

        $sql = "INSERT INTO authors $sqlKeys VALUES $sqlValues";

        $this->database->query($sql);

        foreach ($dataTypes as $value) {
            $this->database->bind(":$value", $data["$value"]);
        }

        if ($this->database->execute()) {
            return true;
        } else {
            return false;
        }
    }
}