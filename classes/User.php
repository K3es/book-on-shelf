<?php

if (file_exists("libraries/Database.php") && "libraries/Password.php") {
    require "libraries/Database.php";
    require "libraries/Password.php";
} else if (file_exists("../libraries/Database.php") && "../libraries/Password.php") {
    require "../libraries/Database.php";
    require "../libraries/Password.php";
} else if (file_exists("../../libraries/Database.php") && "../../libraries/Password.php") {
    require "../../libraries/Database.php";
    require "../../libraries/Password.php";
}

class User
{
    private $database;
    private $passwordLib;

    public function __construct()
    {
        $this->database = new Database;
        $this->passwordLib = new Password;
    }

    public function readUsers()
    {
        $this->database->query("SELECT * FROM users");

        $rows = $this->database->resultSet();

        if ($this->database->rowCount() > 0) {
            return $rows;
        } else {
            return array();
        }
    }

    public function readUserById($userId)
    {
        $this->database->query("SELECT * FROM users WHERE userId = :userId");
        $this->database->bind(":userId", $userId);

        $row = $this->database->resultSingle();

        if ($this->database->rowCount() > 0) {
            return $row;
        } else {
            return false;
        }
    }

    public function readUserByEmail($email)
    {
        $this->database->query("SELECT * FROM users WHERE email = :email");
        $this->database->bind(":email", $email);

        $row = $this->database->resultSingle();

        if ($this->database->rowCount() > 0) {
            return $row;
        } else {
            return false;
        }
    }

    public function deleteUser($userId) {
        $this->database->query("DELETE FROM users WHERE userId = :userId");
        $this->database->bind(":userId", $userId);

        $this->database->execute();
    }

    public function login($email, $password)
    {
        $this->database->query("SELECT * FROM users WHERE email = :email");
        $this->database->bind(":email", $email);

        $row = $this->database->resultSingle();
        $hashedPassword = $row->password;

        if ($this->passwordLib->verify($password, $hashedPassword)) {
            return $row;
        } else {
            return false;
        }
    }


    public function updateUser($userId, $data) {
        $sqlSet = "";
        $userPassword = $this->readUserById($userId)->password;

        foreach ($data as $key => $value) {
            if (!is_null($value)) {
                $sqlSet = "$sqlSet$key = :$key, ";
            }
        }

        $sqlSet = rtrim($sqlSet, ", ");

        $sql = "UPDATE users SET $sqlSet WHERE userId = :userId";

        $this->database->query($sql);

        if (isset($data["password"]) && $userPassword !== $data["password"]) {
            $data["password"] = $this->passwordLib->hash($data["password"]);
        }

        foreach ($data as $key => $value) {
            $this->database->bind(":$key", $value);
        }

        $this->database->bind(":userId", $userId);

        if ($this->database->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function createUser($data)
    {
        $sqlKeys = "";
        $sqlValues = "";
        $dataTypes = array(
            "firstName",
            "middleName",
            "lastName",
            "gender",
            "birthday",
            "email",
            "password",
            "cityName",
            "streetName",
            "homeAddress",
            "zipCode"
        );

        foreach ($dataTypes as $value) {
            $sqlKeys = "$sqlKeys$value, ";
            $sqlValues = "$sqlValues:$value, ";
        }

        $sqlKeys = rtrim($sqlKeys, ", ");
        $sqlKeys = "($sqlKeys)";

        $sqlValues = rtrim($sqlValues, ", ");
        $sqlValues = "($sqlValues)";

        $sql = "INSERT INTO users $sqlKeys VALUES $sqlValues";

        $this->database->query($sql);

        $data["password"] = $this->passwordLib->hash($data["password"]);

        foreach ($dataTypes as $value) {
            $this->database->bind(":$value", $data["$value"]);
        }

        if ($this->database->execute()) {
            return true;
        } else {
            return false;
        }
    }
}