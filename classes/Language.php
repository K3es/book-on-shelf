<?php

if (file_exists("libraries/Database.php")) {
    require "libraries/Database.php";
} else if (file_exists("../libraries/Database.php")) {
    require "../libraries/Database.php";
} else if (file_exists("../../libraries/Database.php")) {
    require "../../libraries/Database.php";
}

class Language
{
    private $database;

    public function __construct()
    {
        $this->database = new Database;
    }

    public function readLanguages()
    {
        $this->database->query("SELECT * FROM languages");

        $rows = $this->database->resultSet();

        if ($this->database->rowCount() > 0) {
            return $rows;
        } else {
            return array();
        }
    }

    public function readLanguageById($languageId)
    {
        $this->database->query("SELECT * FROM languages WHERE languageId = :languageId");
        $this->database->bind(":languageId", $languageId);

        $row = $this->database->resultSingle();

        if ($this->database->rowCount() > 0) {
            return $row;
        } else {
            return false;
        }
    }

    public function deleteLanguage($languageId)
    {
        $this->database->query("DELETE FROM languages WHERE languageId = :languageId");
        $this->database->bind(":languageId", $languageId);

        $this->database->execute();
    }

    public function updateLanguage($languageId, $languageName)
    {
        $this->database->query("UPDATE languages SET languageName = :languageName WHERE languageId = :languageId");
        $this->database->bind(":languageId", $languageId);
        $this->database->bind(":languageName", $languageName);

        if ($this->database->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function createLanguage($languageName)
    {
        $this->database->query("INSERT INTO languages (languageName) VALUES (:languageName)");
        $this->database->bind(":languageName", $languageName);

        if ($this->database->execute()) {
            return true;
        } else {
            return false;
        }
    }
}