<?php

class Database
{
    private $databaseHost;  // Contains active database host
    private $statement;     // Contains prepared query
    private $error;         // Contains handled exception

    // Create __construct upon class creation
    public function __construct()
    {
        // Attempt creation of database host
        try {
            $this->databaseHost = new PDO("mysql:host=localhost;dbname=bookonshelf", "root");

            $this->databaseHost->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $exception) {
            $this->error = $exception->getMessage();
            echo $this->error;
        }

    }

    // Make prepare query function
    public function query($sql)
    {
        $this->statement = $this->databaseHost->prepare($sql);
    }

    // Make binding function
    public function bind($param, $value, $type = null)
    {
        if (is_null($type)) { // Check if $type is null
            switch (true) { // Use switch to prevent nesting
                case is_int($value): // Check if type integer
                    $type = PDO::PARAM_INT;
                    break;
                case is_bool($value): // Check if type boolean
                    $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value): // Check if type null
                    $type = PDO::PARAM_NULL;
                    break;
                default: // Not integer, boolean, null? Default to string
                    $type = PDO::PARAM_STR;
            }
        }

        // Carefully bind value to prepared statement
        $this->statement->bindValue($param, $value, $type);
    }

    // Make statement executing function
    public function execute()
    {
        return $this->statement->execute();
    }

    // Make function returning fetchAll object
    public function resultSet() {
        $this->execute();
        return $this->statement->fetchAll(PDO::FETCH_OBJ);
    }

    // Make function returning single fetch object
    public function resultSingle() {
        $this->execute();
        return $this->statement->fetch(PDO::FETCH_OBJ);
    }

    // Make function returning total row count
    public function rowCount()
    {
        return $this->statement->rowCount();
    }
}