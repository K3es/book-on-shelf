<?php

class Password
{
    // Hash password using bCrypt
    public function hash($password)
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }

    // Verify password with hash using bCrypt
    public function verify($password, $hash)
    {
        return password_verify($password, $hash);
    }
}