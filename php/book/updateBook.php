<?php
require "../../classes/Book.php";

$book = new Book();
$userId = $_SESSION["loggedUserId"];

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $bookId = $_POST["bookId"];
    $currentBook = $book->readBookById($bookId);
    $stock = $currentBook->stock;
    $reservedStock = $currentBook->reservedStock;
    $usedStock = $currentBook->usedStock;
    $destination = "";

    if (isset($_POST["event"])) {
        if ($_POST["event"] == "rent") {
            $usedStock += 1;

            if ($stock >= 1) {
                $stock -= 1;
            }

            $destination = "rentedbooks";
            $book->createRental($bookId, $userId);
        } else if ($_POST["event"] == "reserve") {
            $reservedStock += 1;

            $destination = "reservedbooks";
            $book->createReserve($bookId, $userId);
        }
    }

    $data = array(
        "bookName" => $_POST["bookName"] ?? $currentBook->bookName,
        "fkAuthorId" => $_POST["authorId"] ?? $currentBook->fkAuthorId,
        "fkGenreId" => $_POST["genreId"] ?? $currentBook->fkGenreId,
        "isbn" => $_POST["isbn"] ?? $currentBook->isbn,
        "fkLanguageId" => $_POST["languageId"] ?? $currentBook->fkLanguageId,
        "pages" => $_POST["pages"] ?? $currentBook->pages,
        "stock" => $_POST["stock"] ?? $stock,
        "usedStock" => $usedStock,
        "reservedStock" => $reservedStock
    );

    if ($_SESSION["isAdmin"] ?? false) {
        $book->updateBook($bookId, $data);
        header("location: ../../index.php?page=books");
    } else if (isset($_POST["event"])) {
        $book->updateBook($bookId, $data);
        header("location: ../../index.php?page=$destination");
    } else {
        header("location: ../../index.php?page=login");
    }
}