<?php
require "../../classes/Book.php";

$book = new Book();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $bookId = $_POST["bookId"] ?? 1;
    $currentBook = $book->readBookById($bookId);

    if (isset($_POST["event"])) {
        if ($_POST["event"] == "return") {
            $rentalId = $_POST["rentalId"];
            $stock = $currentBook->stock + 1;
            $usedStock = $currentBook->usedStock - 1;

            if (isset($book->readReservedBooks($bookId)[0])) {
                $reservedStock = $currentBook->reservedStock - 1;
                $stock -= 1;
                $usedStock += 1;

                $reservedBook = $book->readReservedBooks($bookId)[0];
                echo $reservedBook->reserveId;
                $book->deleteReserve($reservedBook->reserveId);
                $book->createRental($bookId, $reservedBook->userId);
            }

            $data = array(
                "bookName" => $currentBook->bookName,
                "fkAuthorId" => $currentBook->fkAuthorId,
                "fkGenreId" => $currentBook->fkGenreId,
                "isbn" => $currentBook->isbn,
                "fkLanguageId" => $currentBook->fkLanguageId,
                "pages" => $currentBook->pages,
                "stock" => $stock,
                "usedStock" => $usedStock,
                "reservedStock" => $reservedStock ?? $currentBook->reservedStock
            );

            $book->deleteRental($rentalId);
            $book->updateBook($bookId, $data);

            header("location: ../../index.php?page=rentedbooks");
        } else if ($_POST["event"] == "cancel") {
            $reserveId = $_POST["reserveId"];
            $reservedStock = $currentBook->reservedStock - 1;

            $data = array(
                "bookName" => $currentBook->bookName,
                "fkAuthorId" => $currentBook->fkAuthorId,
                "fkGenreId" => $currentBook->fkGenreId,
                "isbn" => $currentBook->isbn,
                "fkLanguageId" => $currentBook->fkLanguageId,
                "pages" => $currentBook->pages,
                "stock" => $currentBook->stock,
                "usedStock" => $currentBook->usedStock,
                "reservedStock" => $reservedStock
            );

            $book->deleteReserve($reserveId);
            $book->updateBook($bookId, $data);
            header("location: ../../index.php?page=reservedbooks");
        }
    } else if (isset($_SESSION["isAdmin"]) && $_SESSION["isAdmin"]) {
        $book->deleteBook($bookId);
        header("location: ../../index.php?page=books");
    } else {
        header("location: ../../index.php?page=login");
    }
}