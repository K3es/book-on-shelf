<?php
require "../../classes/Book.php";

$book = new Book();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $data = array(
        "bookName" => $_POST["bookName"],
        "fkAuthorId" => $_POST["authorId"],
        "fkGenreId" => $_POST["genreId"],
        "isbn" => $_POST["isbn"],
        "fkLanguageId" => $_POST["languageId"],
        "pages" => $_POST["pages"],
        "stock" => $_POST["stock"],
        "usedStock" => 0,
        "reservedStock" => 0
    );

    if (isset($_SESSION["isAdmin"]) && $_SESSION["isAdmin"]) {
        $book->createBook($data);
        header("location: ../../index.php?page=books");
    } else {
        header("location: ../../index.php?page=login");
    }
}