<?php
require "../../classes/Language.php";

$language = new Language();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $languageId = $_POST["languageId"];
    $languageName = $_POST["languageName"];

    if (isset($_SESSION["isAdmin"]) && $_SESSION["isAdmin"]) {
        $language->deleteLanguage($languageId);
        header("location: ../../index.php?page=languages");
    } else {
        header("location: ../../index.php?page=login");
    }
}
