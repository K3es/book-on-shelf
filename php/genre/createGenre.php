<?php
require "../../classes/Genre.php";

$genre = new Genre();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $genreName = $_POST["genreName"];

    if (isset($_SESSION["isAdmin"]) && $_SESSION["isAdmin"]) {
        $genre->createGenre($genreName);
        header("location: ../../index.php?page=genres");
    } else {
        header("location: ../../index.php?page=login");
    }
}