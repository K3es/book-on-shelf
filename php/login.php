<?php
require "../classes/User.php";

$user = new User();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $email = $_POST["email"];
    $password = $_POST["password"];

    if ($user->login($email, $password)) {
        $userObject = $user->readUserByEmail($email);

        $_SESSION["loggedEmail"] = $userObject->email;
        $_SESSION["loggedUserId"] = $userObject->userId;
        $_SESSION["loggedIn"] = true;
        $_SESSION["isAdmin"] = $userObject->isAdmin ? true : false;

        header("location: ../index.php?page=home");
    } else {
        $_SESSION["loginError"] = "Invalid credentials.";

        header("location: ../index.php?page=login");
    }
}