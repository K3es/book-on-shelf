<?php
require "../../classes/Author.php";

$author = new Author();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $authorId = $_POST["authorId"];
    $data = array(
        "firstName" => $_POST["firstName"],
        "middleName" => $_POST["middleName"],
        "lastName" => $_POST["lastName"]
    );

    if (isset($_SESSION["isAdmin"]) && $_SESSION["isAdmin"]) {
        $author->updateAuthor($authorId, $data);
        header("location: ../../index.php?page=authors");
    } else {
        header("location: ../../index.php?page=login");
    }
}