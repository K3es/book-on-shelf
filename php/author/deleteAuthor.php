<?php
require "../../classes/Author.php";

$author = new Author();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $authorId = $_POST["authorId"];

    if (isset($_SESSION["isAdmin"]) && $_SESSION["isAdmin"]) {
        $author->deleteAuthor($authorId);
        header("location: ../../index.php?page=authors");
    } else {
        header("location: ../../index.php?page=login");
    }
}
