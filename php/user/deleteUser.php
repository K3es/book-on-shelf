<?php
require "../../classes/User.php";

$user = new User();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $userId = $_POST["userId"];

    if (isset($_SESSION["isAdmin"]) && $_SESSION["isAdmin"]) {
        $user->deleteUser($userId);
        header("location: ../../index.php?page=users");
    } else {
        header("location: ../../index.php?page=login");
    }
}