<?php
require "../../classes/User.php";

$user = new User();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $userId = $_POST["userId"];

    $data = array(
        "firstName" => $_POST["firstName"],
        "middleName" => $_POST["middleName"],
        "lastName" => $_POST["lastName"],
        "gender" => $_POST["gender"],
        "birthday" => $_POST["birthday"],
        "email" => $_POST["email"],
        "password" => $_POST["password"],
        "cityName" => $_POST["city"],
        "streetName" => $_POST["streetName"],
        "homeAddress" => $_POST["homeAddress"],
        "zipCode" => $_POST["zipCode"]
    );

    if (isset($_SESSION["isAdmin"]) && $_SESSION["isAdmin"]) {
        $user->updateUser($userId, $data);
        header("location: ../../index.php?page=users");
    } else {
        header("location: ../../index.php?page=login");
    }
}